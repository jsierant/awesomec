local awful = require("awful")
local wibox = require("wibox")
local beautiful = require("beautiful")

local mouse_buttons = require('mouse_buttons')
local multiscreentag = require('multiscreentag')
local cpu_monitor = require('cpu_monitor')
local mem_monitor = require('mem_monitor')
local audio_volume = require('pulse')
local tags = require('tags')
-- local battery_widget = require("battery-widget")
-- local rmblkmenu = require("rmblkmenu")

local function client_menu_toggle_fn()
    local instance = nil

    return function ()
        if instance and instance.wibox.visible then
            instance:hide()
            instance = nil
        else
            instance = awful.menu.clients({ theme = { width = 250 } })
        end
    end
end

local tasklist_buttons = awful.util.table.join(
  awful.button(
    { }, mouse_buttons.left,
    function (c)
      if c == client.focus then
          c.minimized = true
      else
          -- Without this, the following
          -- :isvisible() makes no sense
          c.minimized = false
          if not c:isvisible() and c.first_tag then
              c.first_tag:view_only()
          end
          -- This will also un-minimize
          -- the client, if needed
          client.focus = c
          c:raise()
      end
    end
  ),
  awful.button(
    { }, mouse_buttons.right,
    client_menu_toggle_fn()
  ),
  awful.button(
    { }, mouse_buttons.middle,
    function(c)
      c:kill()
    end
  ),
  awful.button(
    { }, mouse_buttons.down,
    function ()
      awful.client.focus.byidx(1)
    end
  ),
  awful.button(
    { }, mouse_buttons.up,
    function ()
      awful.client.focus.byidx(-1)
    end
  )
)

local function create_bat_widget()
  return battery_widget {
    adapter = "BATC",
    ac_prefix = "AC",
    battery_prefix = " ",
    limits = {
        { 25, "red"   },
        { 50, "orange"},
        {100, "green" }
    },
    listen = true,
    timeout = 10,
    widget_text = "${AC_BAT}${color_on}${percent}%${color_off}",
    tooltip_text = "Battery ${state}${time_est}\nCapacity: ${capacity_percent}%",
    alert_threshold = 5,
    alert_timeout = 0,
    alert_title = "Low battery !",
    alert_text = "${AC_BAT}${time_est}"
  }
end

function create_layout_box_widget(screen)
  layoutbox = awful.widget.layoutbox(screen)
  layoutbox:buttons(
    awful.util.table.join(
      awful.button({ }, mouse_buttons.left,
                   function () awful.layout.inc( 1) end),
      awful.button({ }, mouse_buttons.right,
                   function () awful.layout.inc(-1) end),
      awful.button({ }, mouse_buttons.down,
                   function () awful.layout.inc( 1) end),
      awful.button({ }, mouse_buttons.up,
                   function () awful.layout.inc(-1) end)
    )
  )
  return layoutbox
end

function create_wibar(screen)
  return awful.wibar({
    position = "top",
    screen = screen,
    height = beautiful.height
  })
end

seperator = wibox.widget{
  text   = '  ',
  align  = 'center',
  valign = 'center',
  widget = wibox.widget.textbox
}



function add_primary_wibar(screen)
  tags.add(screen)
  screen.layoutbox = create_layout_box_widget(screen)
  screen.taglist = tags.create_widget(screen)
  screen.promptbox = awful.widget.prompt()
  screen.wibar = create_wibar(screen)
  screen.calendar = awful.widget.calendar_popup.month(
    { week_numbers = true,
      font = beautiful.font })
  local clock = wibox.widget.textclock()
  screen.calendar:attach(clock)
  screen.wibar:setup {
    layout = wibox.layout.align.horizontal,
    { -- Left widgets
      layout = wibox.layout.fixed.horizontal,
      menu.launcher,
      screen.taglist,
      screen.layoutbox,
      screen.promptbox,
    },
    screen.tasklist, -- Middle widget
    { -- Right widgets
      layout = wibox.layout.fixed.horizontal,
      seperator,
      cpu_monitor.widget,
      mem_monitor.widget,
--       rmblkmenu.button(),
--       create_bat_widget(),
      seperator,
      audio_volume.widget,
      wibox.widget.systray(),
      seperator,
      clock
    },
  }
end

function add_secondary_wibar(screen)
  tags.add(screen)
  screen.layoutbox = create_layout_box_widget(screen)
  screen.taglist = tags.create_widget(screen)
  screen.promptbox = awful.widget.prompt()
  screen.wibar = create_wibar(screen)
  screen.wibar:setup {
    layout = wibox.layout.align.horizontal,
    { -- Left widgets
      layout = wibox.layout.fixed.horizontal,
      screen.taglist,
      screen.layoutbox,
      screen.promptbox,
    },
    screen.tasklist, -- Middle widget
    { -- Right widgets
      layout = wibox.layout.fixed.horizontal
    },
  }
end

awful.screen.connect_for_each_screen(
function(s)
s.tasklist =
    awful.widget.tasklist(
    s,
    awful.widget.tasklist.filter.currenttags,
    tasklist_buttons
    )
if s == screen.primary then
    add_primary_wibar(s)
    return
end
add_secondary_wibar(s)
end)
