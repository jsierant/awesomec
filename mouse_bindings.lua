local awful = require("awful")

local mouse_buttons = require('mouse_buttons')
local menu = require('menu')

root.buttons(awful.util.table.join(
    awful.button({ }, mouse_buttons.right, function () menu.main:toggle() end),
    awful.button({ }, mouse_buttons.down, awful.tag.viewnext),
    awful.button({ }, mouse_buttons.up, awful.tag.viewprev)
))
