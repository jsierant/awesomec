
local keys = require("keys")
local sys = require("sys")
local awful = require("awful")

defaultapps = {}

defaultapps.terminal = "termite"
defaultapps.editor = os.getenv("VISUAL") or "nvim-qt"
defaultapps.browser = os.getenv("BROWSER") or "firefox"
defaultapps.sysmonitor = defaultapps.terminal .. ' --title "System monitor" -e htop'
defaultapps.audiocontrol = "pavucontrol"
defaultapps.filebrowser = defaultapps.terminal .. ' --title "MC" -e mc'
defaultapps.printscreen = 'xsnap'

defaultapps.shortcuts =  awful.util.table.join(
  awful.key({ keys.mod, "Control" }, "w",            function() sys.run(defaultapps.browser)  end),
  awful.key({ keys.mod, "Control" }, "e",            function() sys.run(defaultapps.editor)   end),
  awful.key({ keys.mod            }, "Return",       function() sys.run(defaultapps.terminal) end),
  awful.key({ keys.mod, "Control" }, "f",            function() sys.run(defaultapps.filebrowser) end),
  awful.key({                     }, "XF86HomePage", function() sys.run(defaultapps.browser) end),
  awful.key({                     }, "Print",        function() sys.run(defaultapps.printscreen) end)
)

return defaultapps
