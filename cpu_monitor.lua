local awful = require("awful")
local wibox = require("wibox")
local gears = require("gears")

local mouse_buttons = require('mouse_buttons')
local defaultapps = require('defaultapps')
local sys = require('sys')
local cpu = require('hwparts.cpu')


local cpu_monitor = {
    widget = nil
}

cpu_monitor.widget = wibox.widget {
    max_value = 100,
    step_width = 3,
    step_spacing = 1,
    widget = wibox.widget.graph
}

cpu_monitor.widget:buttons(
    awful.util.table.join(
        awful.button({}, mouse_buttons.left,
            function ()
                sys.run(defaultapps.sysmonitor)
            end
        )
    )
)

local timer =
  gears.timer {
    timeout = 1
  }

timer:connect_signal(
  'timeout',
    function()
      local current = cpu.core.total(cpu.state().total)
      cpu_monitor.widget:add_value(current)
    end
)

timer:start()

return cpu_monitor
