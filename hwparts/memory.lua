local memory = {}

local meminfo_file_name = "/proc/meminfo"

local function set_val_if_present(line, name, set)
  local val = string.match(line, name .. ":%s+(%d+)%s+kB")
  if val then set(tonumber(val)) end
end

local function calc_used(state)
  return memory.total()
    - state.free
    - state.buffers
    - state.cached
end

--- Get memory state
--
-- All values in kB.
-- @treturn {free=number,buffers=number,cached=number,used=number}
function memory.state()
  local state = {}
  for line in io.lines(meminfo_file_name) do
    set_val_if_present(line, "MemFree", function(val) state.free = val end)
    set_val_if_present(line, "Buffers", function(val) state.buffers = val end)
    set_val_if_present(line, "Cached", function(val) state.cached = val end)
    if state.free and state.buffers and state.cached then break end
    end
    state.used = calc_used(state)
  return state
end

local total = nil

--- Get total memory size in kB
-- @treturn number
function memory.total()
  if not total then
    for line in io.lines(meminfo_file_name) do
      set_val_if_present(line, "MemTotal", function(val) total = val end)
      if total then break end
    end
  end
  return total
end

return memory
