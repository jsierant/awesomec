local naughty = require("naughty")

local logger = {}

function logger.debug(t)
  naughty.notify({ title = "DEBUG", text = t })
end

function logger.error(t)
  naughty.notify({
      preset = naughty.config.presets.critical,
      title = "ERROR",
      text = t })
end


function logger.info(t)
  naughty.notify({ title = "INFO", text = t })
end

return logger
