local awful = require('awful')
local gears = require('gears')

local multiscreentag = {}
local logger = require('logger')
function multiscreentag.viewprev()
	for s in screen do
		awful.tag.viewprev(s)
	end
end

function multiscreentag.viewnext()
	for s in screen do
		awful.tag.viewnext(s)
	end
end

function multiscreentag.view(t)
	for _, t in ipairs(multiscreentag.tags(t)) do
		t:view_only()
	end
end

function multiscreentag.tags(t)
	local tags = {}
	for s in screen do
		tags[#tags + 1] = s.tags[t.index]
	end
	return tags
end

function multiscreentag.viewtoggle(t)
	for _, t in ipairs(multiscreentag.tags(t)) do
		awful.tag.viewtoggle(t)
	end
end

function multiscreentag.selectedtags(s)
	local allselectedtags = {}
	for _, selectedtag in pairs(s.selected_tags) do
		for _, t in ipairs(multiscreentag.tags(selectedtag)) do
			allselectedtags[#allselectedtags + 1] = t
		end
	end
	return allselectedtags
end

function multiscreentag.tagclient(c, tag)
	for _, t in ipairs(multiscreentag.tags(tag or awful.screen.focused().selected_tag)) do
		if c.first_tag == t then
			return true
		end
	end
	return false
end

function multiscreentag.delete_by_name(name)
  tags = {}
  for _, t in pairs(root.tags()) do
    if t.name == name then
      for _, c in pairs(t:clients()) do
        c:kill()
      end
      tags[#tags + 1] = t
    end
  end

  gears.timer.weak_start_new(
    0.1,
    function()
      for _, t in pairs(tags) do
        awful.tag.viewtoggle(t)
        t:delete()
      end
    end
  )
end

function multiscreentag.add(name, callback)
  local callback = callback or function(_) end
  for s in screen do
    callback(
      awful.tag.add(
        name,
        { screen=s,
          layout = awful.layout.suit.max }
      )
    )
    local new_tag = awful.tag.find_by_name(s, name)
    new_tag.index = s.selected_tag.index+1
  end
end


return multiscreentag

