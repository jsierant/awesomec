
local naughty = require("naughty")

local in_error = false

if awesome.startup_errors then
    naughty.notify({ preset = naughty.config.presets.critical,
                        title = "STARTUP ERROR",
                        text = awesome.startup_errors })
end

do
    awesome.connect_signal("debug::error", function (err)
        if in_error then
            return
        end

        in_error = true
        naughty.notify({ preset = naughty.config.presets.critical,
                        title = "RUNTIME ERROR",
                        text = tostring(err) })
        in_error = false
    end)
end
