local utils = require('hwparts.utils')
local logger = require("logger")

local blk = {}

local list_bkl_cmd='lsblk -r --output RM,TYPE,UUID,SIZE,NAME,LABEL|egrep "^1 part"'

local function parse(line)
  local split = utils.split(line, "%s+")
  return { uuid = split[3], size = split[4], name = split[5], label = split[6] }
end

local function uuid_dev(uuid)
  return string.format('/dev/disk/by-uuid/%s', uuid)
end

local function mount_target(uuid)
  local mpfile = io.popen(string.format(
    'findmnt --output TARGET --pairs %s | cut -d\'"\' -f2',
    uuid_dev(uuid)))
  if not mpfile then return false, nil end

  local mp = mpfile:read('*all')
  mpfile:close()
  if string.len(mp) > 0 then
    return true, mp
  end
  return true, nil
end

function blk.list()
  local lsblk = io.popen(list_bkl_cmd)
  if not lsblk then return nil end
  local result = utils.split(lsblk:read('*all'), '\n')
  lsblk:close()
  local blocks = {}
  for _,line in ipairs(result) do
    local info = parse(line)
    local success, mt = mount_target(info.uuid)
    if not success then return nil end
    info.mount_target = mt
    table.insert(blocks, info)
  end
  return blocks
end

function blk.mount(b)
  local m = io.open('udisksctl mount --block-device ' .. uuid_dev(b.uuid))
  if not m then return false end
  m:close()
  return true
end

function blk.umount(b)
  local um = io.open('udisksctl unmount --block-device ' .. uuid_dev(b.uuid))
  if not um then return false end
  um:close()
  return true
end


function blk.mounted(block)
  return block.mount_target ~= nil
end

return blk
