local awful = require("awful")
local keys = require("keys")
local beautiful = require("beautiful")
local mouse_buttons = require('mouse_buttons')

local function clear_client_state(c)
  c.maximized = false
  c.fullscreen = false
  c.minimized = false
  c.maximized_vertical = false
  c.maximized_horizontal = false
end

local clientkeys = awful.util.table.join(
  awful.key({ keys.mod,           }, "m",
    function (c)
      if c.fullscreen then
        clear_client_state(c)
        c:raise()
        return
      end
      if c.maximized then
        c.maximized = false
        c.fullscreen = true
        c:raise()
        return
      end
      clear_client_state(c)
      c.maximized = true
      c:raise()
    end,
    { description = "switch window size(normal->maximize->fullscreen)",
      group = "client" }
  ),

  awful.key({ keys.mod, 'Shift'}, "m",
    function (c)
      if c.fullscreen then
        c.fullscreen = false
        c.maximized = true
        c:raise()
        return
      end
      if c.maximized then
        clear_client_state(c)
        c:raise()
        return
      end
      clear_client_state(c)
      c.minimized = true
    end,
    { description = "switch window size(fullscreen->maximize->normal->minimize)",
      group = "client" }
  ),

  awful.key({ keys.mod, "Shift"   }, "c",
    function(c)
      c:kill()
    end,
    { description = "close",
      group = "client" }
  ),
  awful.key({ keys.mod }, "f",
    awful.client.floating.toggle,
    { description = "toggle floating",
      group = "client" }
  ),

  awful.key({ keys.mod, "Control" }, "Return",
    function (c)
      c:swap(awful.client.getmaster())
    end,
    {description = "move to master", group = "client"}
  ),
  awful.key({ keys.mod,           }, "o",
    function (c)
      c:move_to_screen()
    end,
    {description = "move to screen", group = "client"}
  ),
  awful.key({ keys.mod,           }, "t",
    function (c)
      c.ontop = not c.ontop
    end,
    {description = "toggle keep on top", group = "client"}
  )
)

local clientbuttons = awful.util.table.join(
    awful.button({ }, mouse_buttons.left, function (c) client.focus = c; c:raise() end),
    awful.button({ keys.mod }, mouse_buttons.left, awful.mouse.client.move),
    awful.button({ keys.mod }, mouse_buttons.right, awful.mouse.client.resize))

-- Rules to apply to new clients (through the "manage" signal).
awful.rules.rules = {
    -- All clients will match this rule.
    { rule = { },
      properties = { border_width = beautiful.border_width,
                     border_color = beautiful.border_normal,
                     focus = awful.client.focus.filter,
                     raise = true,
                     keys = clientkeys,
                     buttons = clientbuttons,
                     screen = awful.screen.preferred,
                     placement = awful.placement.no_overlap+awful.placement.no_offscreen
     }
    },

    -- Floating clients.
    { rule_any = {
        instance = {
          "DTA",  -- Firefox addon DownThemAll.
          "copyq",  -- Includes session name in class.
        },
        class = {
          "Google Play Music Desktop Player",
          "Arandr",
          "Gpick",
          "Kruler",
          "MessageWin",  -- kalarm.
          "Sxiv",
          "Wpa_gui",
          "pinentry",
          "veromix",
          "xtightvncviewer",
          "Pavucontrol"
        },

        name = {
          "Event Tester",  -- xev.
          "Find Subtitles",
          "Login - DOORS",
          "Find and Replace:",
          "DOORS",
          "Find:",
          "System monitor"
        },
        role = {
          "AlarmWindow",  -- Thunderbird's calendar.
          "pop-up",       -- e.g. Google Chrome's (detached) Developer Tools.
        }
      }, properties = { floating = true }},

    -- Add titlebars to normal clients and dialogs
    { rule_any = {type = { "normal", "dialog" }
      }, properties = { titlebars_enabled = true }
    },
}
-- }}}

-- {{{ Signals
-- Signal function to execute when a new client appears.
client.connect_signal("manage", function (c)
    -- Set the windows at the slave,
    -- i.e. put it at the end of others instead of setting it master.
    -- if not awesome.startup then awful.client.setslave(c) end

    if awesome.startup and
      not c.size_hints.user_position
      and not c.size_hints.program_position then
        -- Prevent clients from being unreachable after screen count changes.
        awful.placement.no_offscreen(c)
    end
end)

-- {{{ Window borders
client.connect_signal(
  "focus",
  function(c)
    c.border_color = beautiful.border_focus
  end
)

client.connect_signal(
  "unfocus",
  function(c)
    c.border_color = beautiful.border_normal
  end
)

client.connect_signal(
  "request::titlebars",
  function(c)
    awful.titlebar(c, { size = 3 })
  end
)
