local wibox = require("wibox")
local gears = require("gears")
local beautiful = require("beautiful")
local awful = require("awful")

local sys = require('sys')
local mouse_buttons = require('mouse_buttons')
local defaultapps = require('defaultapps')
local memory = require('hwparts.memory')

local mem_monitor = {}
mem_monitor.widget =  wibox.widget {
          colors = { '#ff9999', '#99ff99', '#9999ff' },
          min_value    = 0,
          max_value    = memory.total(),
          rounded_edge = false,
          bg           = beautiful.bg_focus,
          border_width = 0.5,
          border_color = "#000000",
          widget       = wibox.container.arcchart
}

mem_monitor.widget:buttons(
    awful.util.table.join(
        awful.button({}, mouse_buttons.left,
            function ()
                sys.run(defaultapps.sysmonitor)
            end
        )
    )
)

local timer = gears.timer { timeout = 1 }

timer:connect_signal(
  'timeout',
    function()
      local state = memory.state()
      mem_monitor.widget.values = { state.used, state.buffers, state.cache }
    end
)

timer:start()

return mem_monitor
