local naughty = require("naughty")

command = { }

local split = function(s, pattern)
    parts = {}
    for part in string.gmatch(s, pattern or "%S+") do parts[#parts + 1] = part end
    return parts
end

local splitCmd = function(cmdline)
    cmdparts = split(cmdline)
    if #cmdparts == 0 then return "", {} end
    return cmdparts[1], {table.unpack(cmdparts, 2, #cmdparts)}
end

local filter = function(tab, pattern)
    filtered = {}
    for _, e in ipairs(tab) do
        if string.match(e, pattern) then
          filtered[#filtered + 1] = e
        end
    end
    return filtered
end

local selectCompletion = function(tocomplete, ncomp, completions)
    if #completions == 0 then return tocomplete end
    while ncomp > #completions do ncomp = ncomp - #completions end
    return completions[ncomp] .. " "
end

local calcPos = function(newLine)
    return #newLine + 2
end

local splitArgs = function(args, inthemiddle)
    if inthemiddle then return args[#args], {table.unpack(args, 1, #args-1)} end
    return "", args
end

local warn = function(text)
    naughty.notify({ title = "COMMAND - WARN", text = text })
end

local debug = function(text)
    naughty.notify({ title = "COMMAND - DEBUG", text = text })
end

Commander = {}
Commander.__index = Commander

function Commander.create(cfg)
    local c = {}
    setmetatable(c, Commander)
    c.cfg = cfg
    return c
end

function Commander:run(command)
    cmd, args = splitCmd(command)
    for _, ctx in ipairs(self.cfg) do
--         debug("processing ctx for command: " .. ctx.cmd)
        if ctx.cmd == cmd then
--             debug("running command: " .. ctx.cmd)
            ctx.run(args)
            return
        end
    end
    warn("Unknown command: " .. command)
end

function Commander:complete(command, cpos, ncomp)
    cmd, args = splitCmd(command)
    inthemiddle = #command > 0 and command:sub(#command,#command) ~= " "
    completecmd = (#command == 0) or (inthemiddle and #args == 0)

--     debug("processing command: \"" .. command .. "\"")
--     debug("completion cmd: " .. cmd .. ", args: " .. table.concat(args, "-"))

    if completecmd then
        newLine = selectCompletion(cmd, ncomp, self:commands(cmd))
        return newLine, calcPos(newLine)
    else
        cmdctx = self:cmdCtx(cmd)
        if cmdctx then
            tocomplete, restargs = splitArgs(args, inthemiddle)
            pattern = tocomplete .. "%a*"
            filteredCompletions = filter(cmdctx.completions(), pattern)
            newCmd = cmd
             if #restargs > 0 then  newCmd = newCmd .. " " .. table.concat(restargs, " ") end
             newCmd = newCmd .. " " .. selectCompletion(tocomplete, ncomp, filteredCompletions)
--              debug("filteredCompletions: " .. table.concat(filteredCompletions, "-"))
            return newCmd, calcPos(newCmd)
        end
    end
    return command, cpos
end

function Commander:commands(partialCmd)
    cmds = {}
    for _, ctx in ipairs(self.cfg) do
        if string.match(ctx.cmd, "^" .. (partialCmd or "") .. '%a*') then
            cmds[#cmds + 1] = ctx.cmd
        end
    end
    return cmds
end

function Commander:cmdCtx(cmd)
    for _, ctx in ipairs(self.cfg) do
        if cmd == ctx.cmd then return ctx end
    end
    return nil
end

function command.createCommander(cfg)
    return Commander.create(cfg)
end

return command
