-------------------------------
--  "Zenburn" awesome theme  --
--    By Adrian C. (anrxc)   --
-------------------------------

-- {{{ Main
local theme = {}
local env = require("env")
theme.wallpaper = env.theme_dir .. "/zenburn-background.png"
-- }}}

-- {{{ Styles
theme.font_size = 10
theme.font = "Liberation Mono " .. theme.font_size
theme.height = theme.font_size * 2.6

-- {{{ Colors
theme.fg_normal  = "#DCDCCC"
theme.fg_focus   = "#F0DFAF"
theme.fg_urgent  = "#CC9393"
theme.bg_normal  = "#2c2c2c"
theme.bg_focus   = "#476477"
theme.bg_urgent  = "#3F3F3F"
theme.bg_systray = theme.bg_normal
-- }}}

-- {{{ Borders
theme.border_width  = 2
theme.border_normal = theme.bg_normal
theme.border_focus  = theme.bg_focus
theme.border_marked = theme.fg_urgent
-- }}}

-- {{{ Titlebars
theme.titlebar_bg_focus  = theme.bg_focus
theme.titlebar_bg_normal = theme.bg_normal

-- }}}

-- There are other variable sets
-- overriding the default one when
-- defined, the sets are:
-- [taglist|tasklist]_[bg|fg]_[focus|urgent]
-- titlebar_[normal|focus]
-- tooltip_[font|opacity|fg_color|bg_color|border_width|border_color]
-- Example:
--theme.taglist_bg_focus = "#CC9393"
-- }}}

-- {{{ Widgets
-- You can add as many variables as
-- you wish and access them by using
-- beautiful.variable in your rc.lua
--theme.fg_widget        = "#AECF96"
--theme.fg_center_widget = "#88A175"
--theme.fg_end_widget    = "#FF5656"
--theme.bg_widget        = "#494B4F"
--theme.border_widget    = "#3F3F3F"
-- }}}
theme.graph_fg = theme.fg_normal
theme.graph_bg = theme.bg_normal


-- {{{ Mouse finder
theme.mouse_finder_color = "#CC9393"
-- mouse_finder_[timeout|animate_timeout|radius|factor]
-- }}}

-- {{{ Menu
-- Variables set for theming the menu:
-- menu_[bg|fg]_[normal|focus]
-- menu_[border_color|border_width]
theme.menu_height = theme.height
theme.menu_width  = theme.font_size * 18
-- }}}

-- {{{ Icons
-- {{{ Taglist
theme.taglist_squares_sel   = env.theme_dir .. "/taglist/squarefz.png"
theme.taglist_squares_unsel = env.theme_dir .. "/taglist/squarez.png"
--theme.taglist_squares_resize = "false"
-- }}}

-- {{{ Misc
theme.awesome_icon           = env.theme_dir .. "/awesome-icon.png"
theme.menu_submenu_icon      = "/usr/share/awesome/themes/default/submenu.png"
theme.pen_icon = env.theme_dir .. "/pen.png"
theme.notification_icon_size = 100
-- }}}

-- {{{ Layout
theme.layout_tile       = env.theme_dir .. "/layouts/tile.png"
theme.layout_tileleft   = env.theme_dir .. "/layouts/tileleft.png"
theme.layout_tilebottom = env.theme_dir .. "/layouts/tilebottom.png"
theme.layout_tiletop    = env.theme_dir .. "/layouts/tiletop.png"
theme.layout_fairv      = env.theme_dir .. "/layouts/fairv.png"
theme.layout_fairh      = env.theme_dir .. "/layouts/fairh.png"
theme.layout_spiral     = env.theme_dir .. "/layouts/spiral.png"
theme.layout_dwindle    = env.theme_dir .. "/layouts/dwindle.png"
theme.layout_max        = env.theme_dir .. "/layouts/max.png"
theme.layout_fullscreen = env.theme_dir .. "/layouts/fullscreen.png"
theme.layout_magnifier  = env.theme_dir .. "/layouts/magnifier.png"
theme.layout_floating   = env.theme_dir .. "/layouts/floating.png"
theme.layout_cornernw   = env.theme_dir .. "/layouts/cornernw.png"
theme.layout_cornerne   = env.theme_dir .. "/layouts/cornerne.png"
theme.layout_cornersw   = env.theme_dir .. "/layouts/cornersw.png"
theme.layout_cornerse   = env.theme_dir .. "/layouts/cornerse.png"
-- }}}

-- {{{ Multimedia icons
theme.audio_volume_high = env.theme_dir .. "/audio-volume-high.png"
theme.audio_volume_medium = env.theme_dir .. "/audio-volume-medium.png"
theme.audio_volume_low = env.theme_dir .. "/audio-volume-low.png"
theme.audio_volume_muted = env.theme_dir .. "/audio-volume-muted.png"

-- }}}


-- {{{ Titlebar
theme.titlebar_close_button_focus  = env.theme_dir .. "/titlebar/close_focus.png"
theme.titlebar_close_button_normal = env.theme_dir .. "/titlebar/close_normal.png"

theme.titlebar_minimize_button_normal = "/usr/share/awesome/themes/default/titlebar/minimize_normal.png"
theme.titlebar_minimize_button_focus  = "/usr/share/awesome/themes/default/titlebar/minimize_focus.png"

theme.titlebar_ontop_button_focus_active  = env.theme_dir .. "/titlebar/ontop_focus_active.png"
theme.titlebar_ontop_button_normal_active = env.theme_dir .. "/titlebar/ontop_normal_active.png"
theme.titlebar_ontop_button_focus_inactive  = env.theme_dir .. "/titlebar/ontop_focus_inactive.png"
theme.titlebar_ontop_button_normal_inactive = env.theme_dir .. "/titlebar/ontop_normal_inactive.png"

theme.titlebar_sticky_button_focus_active  = env.theme_dir .. "/titlebar/sticky_focus_active.png"
theme.titlebar_sticky_button_normal_active = env.theme_dir .. "/titlebar/sticky_normal_active.png"
theme.titlebar_sticky_button_focus_inactive  = env.theme_dir .. "/titlebar/sticky_focus_inactive.png"
theme.titlebar_sticky_button_normal_inactive = env.theme_dir .. "/titlebar/sticky_normal_inactive.png"

theme.titlebar_floating_button_focus_active  = env.theme_dir .. "/titlebar/floating_focus_active.png"
theme.titlebar_floating_button_normal_active = env.theme_dir .. "/titlebar/floating_normal_active.png"
theme.titlebar_floating_button_focus_inactive  = env.theme_dir .. "/titlebar/floating_focus_inactive.png"
theme.titlebar_floating_button_normal_inactive = env.theme_dir .. "/titlebar/floating_normal_inactive.png"

theme.titlebar_maximized_button_focus_active  = env.theme_dir .. "/titlebar/maximized_focus_active.png"
theme.titlebar_maximized_button_normal_active = env.theme_dir .. "/titlebar/maximized_normal_active.png"
theme.titlebar_maximized_button_focus_inactive  = env.theme_dir .. "/titlebar/maximized_focus_inactive.png"
theme.titlebar_maximized_button_normal_inactive = env.theme_dir .. "/titlebar/maximized_normal_inactive.png"
-- }}}
-- }}}

return theme

-- vim: filetype=lua:expandtab:shiftwidth=4:tabstop=8:softtabstop=4:textwidth=80
