local keys = require("keys")
local awful = require("awful")
local beautiful = require("beautiful")

local mouse_buttons = require('mouse_buttons')
local multiscreentag = require('multiscreentag')

local tags = {}

for i = 1, 9 do
  globalkeys = awful.util.table.join(globalkeys,
    -- View tag only.
    awful.key(
      { keys.mod }, "#" .. i + 9,
      function ()
            local screen = awful.screen.focused()
            local tag = screen.tags[i]
            if tag then
              multiscreentag.view(tag)
            end
      end,
      {description = "view tag #"..i, group = "tag"}
    ),
    -- Toggle tag display.
    awful.key(
      { keys.mod, "Control" }, "#" .. i + 9,
      function ()
          local screen = awful.screen.focused()
          local tag = screen.tags[i]
          if tag then
              awful.tag.viewtoggle(tag)
          end
      end,
      {description = "toggle tag #" .. i, group = "tag"}
    ),
    -- Move client to tag.
    awful.key(
      { keys.mod, "Shift" }, "#" .. i + 9,
      function ()
        if client.focus then
          local tag = client.focus.screen.tags[i]
          if tag then
            client.focus:move_to_tag(tag)
          end
        end
      end,
      {description = "move focused client to tag #"..i, group = "tag"}
    ),
    -- Toggle tag on focused client.
    awful.key(
      { keys.mod, "Control", "Shift" }, "#" .. i + 9,
      function ()
        if client.focus then
          local tag = client.focus.screen.tags[i]
          if tag then
            client.focus:toggle_tag(tag)
          end
        end
      end,
      {description = "toggle focused client on tag #" .. i, group = "tag"}
    )
  )
end


-- Create a wibox for each screen and add itk
local taglist_buttons = awful.util.table.join(
  awful.button({ }, mouse_buttons.left,
               multiscreentag.view
  ),
  awful.button({ keys.mod }, mouse_buttons.left,
    function(t)
      if client.focus then
        client.focus:move_to_tag(t)
      end
    end
  ),
  awful.button(
    { }, mouse_buttons.right,
    multiscreentag.viewtoggle
  ),
  awful.button(
    { keys.mod }, mouse_buttons.right,
    function(t)
      if client.focus then
        client.focus:toggle_tag(t)
      end
    end
  ),
  awful.button(
    { }, mouse_buttons.down,
    multiscreentag.viewprev
  ),
  awful.button(
    { }, mouse_buttons.up,
    multiscreentag.viewnext
  )
)

tags.add = function(screen, layout)
  awful.tag(
    { "main" },
    screen,
    layout or awful.layout.layouts[2])
end

tags.create_widget = function(screen)
  return awful.widget.taglist(
    screen,
    awful.widget.taglist.filter.all,
    taglist_buttons,
    { font = beautiful.font }
  )
end

return tags
