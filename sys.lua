local awful = require("awful")
local gears = require("gears")
local beautiful = require("beautiful")
local sys = {}

local function run_delayed(d, fun)
  local t = gears.timer({ timeout = d })
  t:connect_signal("timeout", function() t:stop() fun() end)
  t:start()
end

local function run_once(cmd)
  local findme = cmd
  local firstspace = cmd:find(" ")
  if firstspace then
    findme = cmd:sub(0, firstspace-1)
  end
  awful.spawn.with_shell("pgrep -u $USER -x " .. findme .. " > /dev/null || " .. cmd)
end


function sys.reboot()
  awful.spawn("systemctl reboot")
end

function sys.poweroff()
  awful.spawn("systemctl poweroff")
end

function sys.suspend()
  sys.lock_screen()
  awful.spawn("systemctl suspend -i")
end

function sys.lock_screen()
  sys.run("i3lock -t -i " .. beautiful.wallpaper)
end

function sys.run_once(cmd, d)
  if(d ~= nil) then
    run_delayed(d, function() run_once(cmd) end)
  else
    run_once(cmd)
  end
end

function sys.run(cmd, d)
  if(d ~= nil) then
    run_delayed(d, function() awful.spawn(cmd) end)
  else
    awful.spawn.with_shell(cmd)
  end
end

function sys.set_keyboard_map(t)
  awful.spawn("setxkbmap " .. t)
end

function sys.collect_output(cmd)
  local f = io.popen(cmd)
  if f == nil then
    return nil
  end
  local out = f:read("*a")
  f:close()
  return out
end

return sys
