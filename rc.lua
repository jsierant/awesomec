local env = require('env')

local beautiful = require("beautiful")
beautiful.init(env.theme_dir .. "/theme.lua")

local awful = require("awful")
local hotkeys_popup = require("awful.hotkeys_popup").widget

local defaultapps = require('defaultapps')
local keys = require("keys")
local sys = require('sys')

local multiscreentag = require('multiscreentag')

local menu = require('menu')

local audio_volume = require('pulse')

require('errors')
require('mouse_bindings')
require('clients')
require('wallpaper')
require('wibar')


-- Table of layouts to cover with awful.layout.inc, order matters.
awful.layout.layouts = {
    awful.layout.suit.floating,
    awful.layout.suit.tile,
    awful.layout.suit.max,
    awful.layout.suit.tile.bottom,
    awful.layout.suit.spiral.dwindle,
    awful.layout.suit.fair,
    awful.layout.suit.fair.horizontal,
}
-- }}}


-- {{{ Key bindings
local globalkeys = awful.util.table.join(
    defaultapps.shortcuts,
    audio_volume.shortcuts,
    awful.key({ keys.alt,  "Control" }, "l", function() sys.lock_screen() end,
              {description="lock screen", group="display"}),
    awful.key({ keys.mod,           }, "s",      hotkeys_popup.show_help,
              {description="show help", group="awesome"}),
    awful.key({ keys.mod,           }, ",",   multiscreentag.viewprev,
              {description = "view previous", group = "tag"}),
    awful.key({ keys.mod,           }, ".",  multiscreentag.viewnext,
              {description = "view next", group = "tag"}),
    awful.key({ keys.mod,           }, "Escape", awful.tag.history.restore,
              {description = "go back", group = "tag"}),

    -- awful.key({ keys.mod,           }, "j",
    --     function ()
    --         awful.client.focus.byidx( 1)
    --     end,
    --     {description = "focus next by index", group = "client"}
    -- ),
    -- awful.key({ keys.mod,           }, "k",
    --     function ()
    --         awful.client.focus.byidx(-1)
    --     end,
    --     {description = "focus previous by index", group = "client"}
    -- ),
    awful.key({ keys.mod,           }, "w", menu.show,
              {description = "show main menu", group = "awesome"}),

    -- Layout manipulation
    awful.key({ keys.mod, "Shift"   }, "j", function () awful.client.swap.byidx(  1)    end,
              {description = "swap with next client by index", group = "client"}),
    awful.key({ keys.mod, "Shift"   }, "k", function () awful.client.swap.byidx( -1)    end,
              {description = "swap with previous client by index", group = "client"}),
    -- awful.key({ keys.mod, "Control" }, "j", function () awful.screen.focus_relative( 1) end,
    --           {description = "focus the next screen", group = "screen"}),
    -- awful.key({ keys.mod, "Control" }, "k", function () awful.screen.focus_relative(-1) end,
    --           {description = "focus the previous screen", group = "screen"}),
    awful.key({ keys.mod,           }, "u", awful.client.urgent.jumpto,
              {description = "jump to urgent client", group = "client"}),

    -- Standard program
    awful.key({ keys.mod, "Control" }, "r", awesome.restart,
              {description = "reload awesome", group = "awesome"}),
    awful.key({ keys.mod, "Shift"   }, "q", awesome.quit,
              {description = "quit awesome", group = "awesome"}),

    awful.key({ keys.mod,           }, "]",     function () awful.tag.incmwfact( 0.05)          end,
              {description = "increase master width factor", group = "layout"}),
    awful.key({ keys.mod,           }, "[",     function () awful.tag.incmwfact(-0.05)          end,
              {description = "decrease master width factor", group = "layout"}),
    awful.key({ keys.mod,           }, "=",     function () awful.tag.incnmaster( 1, nil, true) end,
              {description = "increase the number of master clients", group = "layout"}),
    awful.key({ keys.mod,           }, "-",     function () awful.tag.incnmaster(-1, nil, true) end,
              {description = "decrease the number of master clients", group = "layout"}),
    awful.key({ keys.mod, "Control" }, "=",     function () awful.tag.incncol( 1, nil, true)    end,
              {description = "increase the number of columns", group = "layout"}),
    awful.key({ keys.mod, "Control" }, "-",     function () awful.tag.incncol(-1, nil, true)    end,
              {description = "decrease the number of columns", group = "layout"}),
    awful.key({ keys.mod,           }, "space", function () awful.layout.inc( 1)                end,
              {description = "select next", group = "layout"}),
    awful.key({ keys.mod, "Shift"   }, "space", function () awful.layout.inc(-1)                end,
              {description = "select previous", group = "layout"}),
    -- Prompt
    awful.key({ keys.mod },            "r",     function () awful.screen.focused().promptbox:run() end,
              {description = "run prompt", group = "launcher"})
)



--- focus
local focus = require("focus")

function multiscreentabnotmin(c, t)
	return not c.minimized and multiscreentag.tagclient(c, t)
end

focus.initRaiseClientOnTagSwitch(multiscreentabnotmin)
focus.initRaiseClientOnSignal("unmanage", multiscreentagnotmin)
focus.initRaiseClientOnSignal("untagged", multiscreentabnotmin)
focus.initRaiseClientOnSignal("property::minimized", multiscreentabnotmin)

globalkeys = awful.util.table.join(globalkeys,
  awful.key(
    { keys.mod,         }, "Tab",
    function(c)
      focus.cycle({direction = 1,
                   modifier = "Super_L",
                   filter = multiscreentag.tagclient})
    end
  ),
  awful.key(
    { keys.mod, "Shift" }, "Tab",
    function(c)
      focus.cycle({direction = -1,
                   modifier = "Super_L",
                   filter = multiscreentag.tagclient,
                   actionKey = "ISO_Left_Tab" })
    end
  )
)

-- dynamic tagging
globalkeys = awful.util.table.join(globalkeys,
  awful.key({ keys.mod }, "t",
    function ()
      awful.prompt.run {
        prompt       = " add tag: ",
        textbox      = awful.screen.focused().promptbox.widget,
        exe_callback =
          function(input)
            if not input or #input == 0 then return end
            multiscreentag.add(
              input,
              function(tag)
                focus.registerTag(multiscreentabnotmin, tag)
              end)
          end,
      }
    end,
    {description = "add", group = "tag"}
  ),
  awful.key({ keys.mod, 'Shift' }, "t",
    function ()
      local t = awful.screen.focused().selected_tag
      if not t then return end
      multiscreentag.delete_by_name(t.name)
    end,
    {description = "remove (kills all tag's clients)", group = "tag"}
  )
)

-- Set keys
root.keys(globalkeys)

sys.set_keyboard_map('pl')

