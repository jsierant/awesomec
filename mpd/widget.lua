local wibox = require("wibox")
local awful = require("awful")
local gears = require("gears")
local mouse_buttons = require("mouse_buttons")

mpd_widget={}

local function start_periodic_timer(on_timeout, p)
    p = p or 10
    local update_widget_timer = gears.timer({ timeout = p })
    update_widget_timer:connect_signal("timeout", on_timeout)
    update_widget_timer:start()
end

function mpd_widget.update(ctrl)
    ctrl.update()
    if ctrl.song == nil then
      song_info = "♫ ►"
      mpd_widget.volume_progressbar:set_width(0)
    else
      song_info = ctrl.song
      mpd_widget.volume_progressbar:set_width(10)
      end
    mpd_widget.song_widget:set_text(song_info)
    mpd_widget.song_progressbar:set_value(ctrl.progress / 100)
    mpd_widget.volume_progressbar:set_value(ctrl.volume / 100)
end

function mpd_widget.create(ctrl, bg_color, wibox_height, p)
  bg_color = bg_color or "#333333"
  local background = wibox.widget.background()
  background:set_bg(bg_color)

  mpd_widget.song_widget = wibox.widget.textbox()
  mpd_widget.song_widget:buttons(awful.util.table.join(
        awful.button({ }, mouse_buttons.left, function() ctrl.toggle_play() end),
        awful.button({ }, mouse_buttons.right, ctrl.show_player),
        awful.button({ }, mouse_buttons.scroll_down, function() ctrl.play_next() end),
        awful.button({ }, mouse_buttons.scroll_up, function() ctrl.play_prev() end)))

  mpd_widget.song_progressbar = awful.widget.progressbar()
  mpd_widget.song_progressbar:set_width(1)
  mpd_widget.song_progressbar:set_height(1)
  mpd_widget.song_progressbar:set_vertical(false)

  mpd_widget.volume_progressbar = awful.widget.progressbar()
  mpd_widget.volume_progressbar:set_background_color("#222222")
  mpd_widget.volume_progressbar:set_width(0)
  mpd_widget.volume_progressbar:set_height(1)
  mpd_widget.volume_progressbar:set_vertical(true)
  local color = { type = "linear", from = { 0, 0 }, to = { 0, wibox_height - 1 },
                      stops = { {0, "#c1230e"}, {0.5, "#c1b20a"}, {1, "#0fc10c" }}}
  mpd_widget.volume_progressbar:set_color(color)
  mpd_widget.volume_progressbar:buttons(awful.util.table.join(
            awful.button({ }, mouse_buttons.scroll_up, function() ctrl.volume_down() end),
            awful.button({ }, mouse_buttons.scroll_down, function() ctrl.volume_up() end)))
  mpd_widget.volume_progressbar:set_ticks(true)

  local seperator = wibox.widget.textbox()
  seperator:set_text(" ")

  local hlayout = wibox.layout.fixed.horizontal()
  hlayout:add(seperator)
  hlayout:add(mpd_widget.song_widget)
  hlayout:add(seperator)
  hlayout:add(mpd_widget.volume_progressbar)
  hlayout:add(seperator)

  local layout = wibox.layout.fixed.vertical()
  layout:add(mpd_widget.song_progressbar)
  layout:add(hlayout)
  background:set_widget(layout)

  mpd_widget.update(ctrl)
  start_periodic_timer(function() mpd_widget.update(ctrl) end, p)
  return background
end

return mpd_widget
