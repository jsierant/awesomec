local naughty = require("naughty")
local awful = require("awful")
local logger = require('logger')
local gears = require("gears")
-- debugging
local printDebug = function(t, c)end

function notifyDebug(t, c)
  local sufix = ""
  if c and c.name then sufix = ": " .. c.name end
  naughty.notify({ title = "DEBUG", text = t .. sufix })
end

-- history
local history = { stack = {}, isRecordingActive = true }

function history.tryRecordFocus(c)
  if history.isRecordingActive then
    history.recordFocus(c)
  end
end

function printclients(list)
    template = '%s\n'
    local tt = { 'reStack:\n' }
    for _,c in ipairs(list) do
        tt[#tt+1]=template:format(c.name)
    end
    logger.error(table.concat(tt))
end

function history.print()
		printclients(history.stack)
end

function history.recordFocus(c)
  printDebug("recording focus", c)
  history.erase(c)
  table.insert(history.stack, c)
end

function history.erase(c)
   for k, v in ipairs(history.stack) do
    if v == c then
      table.remove(history.stack, k)
      return
    end
  end
end

function history.select(f)
  local out = {}
  for _, c in ipairs(history.stack) do
    if f(c) then
			table.insert(out, c) 
		end
  end
  return out
end

function history.findLatest(f)
	clients = history.select(f)
	if #clients > 0 then
		return clients[#clients]
	end
  return nil
end

function history.recordingActive(state)
  history.isRecordingActive = state
end

function history.add(c)
  table.insert(history.stack, c)
end

-- cycler
cyclelist = { list={}, idx = 0 }

function cyclelist.create(list, idx)
  cl = cyclelist
  cl.list = list
  cl.idx = idx
  return cl
end

function cyclelist:cycle(direction)
  newIdx = self.idx + direction
  if newIdx > #self.list+1 then
    newIdx = 1
  end
  if newIdx < 1 then
    newIdx = #self.list
  end
  self.idx = newIdx
end

function cyclelist:get()
  if #self.list == 0 then
    return nil
  end
  return self.list[self.idx]
end


local cycler = { }
local keygrabber = require("awful.keygrabber")

function cycler.run()
    cycler.grabber = keygrabber.run(cycler.cycle)
end

function cycler.raiseClient(c)
    client.focus = c
    c:raise()
end

function cycler.exitGrabber()
  keygrabber.stop(cycler.grabber)
  history.recordingActive(true)
end

function cycler.recordFocusAndExit()
  history.recordFocus(client.focus)
  cycler.exitGrabber()
end

function cycler.onFirstSelect(modifiers, key, event)
  history.recordingActive(false)
  clients = history.select(function(c) return cycler.filter(c) end)
  cycler.selectedClients = cyclelist.create(clients, #clients)
  cycler.selectedClients:cycle(-cycler.direction)

  newClient = cycler.selectedClients:get()
  if newClient == nil then
    cycler.exitGrabber()
  end
  cycler.raiseClient(newClient)
  cycler.handle = cycler.onFollowingSelect
  if event == "release" and key == cycler.modifier then
    cycler.recordFocusAndExit()
  end
end

function cycler.onFollowingSelect(modifiers, key, event)
  if event == "release" and key == cycler.modifier then
    cycler.recordFocusAndExit()
  elseif event == "press" and key == cycler.actionKey then
    cycler.selectedClients:cycle(-cycler.direction)
    cycler.raiseClient(cycler.selectedClients:get())
  end
end

function cycler.cycle(modifiers, key, event)
--   printDebug("grabber: mod: " .. table.concat(modifiers, ',')
--             .. ", key: " .. tostring(key)
--             .. ", event: " .. tostring(event)
--             .. ", modifier_key: " .. tostring(cycler.modifier))
  cycler.handle(modifiers, key, event)
end

-- focus
local function selectedTag(c)
  return c.first_tag == awful.screen.focused().selected_tag
end

local function notMinimized(c) return not c.minimized end

focus = {
  raiseFilter = { onSelectedTag = selectedTag,
                  notMinimizedOnSelectedTag = function(c) return notMinimized(c) and selectedTag(c) end },
  cycleFilter = { onSelectedTag = selectedTag }
}

function focus.enableDebug()
  printDebug = notifyDebug
end

client.connect_signal("unmanage", history.erase)
client.connect_signal("focus", history.tryRecordFocus)

function focus.raise(filter)
    gears.timer.start_new(
        0.05,
        function()
            local c = history.findLatest(filter or focus.raiseFilter.onSelectedTag)
            if c then
                client.focus = c
                client.focus:raise()
                return false
            end
        end
    )
  printDebug("Unable to find client to raise")
end

-- filter: bool(client)
function focus.initRaiseClientOnTagSwitch(filter)
		for _,t in pairs(screen.primary.tags) do
			focus.registerTag(filter, t)
		end
end

function focus.registerTag(filter, tag)
	tag:connect_signal("property::selected",
		function(ta)
			  if ta.selected then
					focus.raise(function(c) 
						return filter(c, ta) 
					end)
				end
	end)
end

function focus.initRaiseClientOnSignal(sigName, filter)
  client.connect_signal(sigName,
    function(c)
        focus.raise(filter or focus.raiseFilter.onSelectedTag)
    end)
end

-- args = { direction = [-1, 1],
--          actionKey="Tab",
--          filter = focus.cycleFilter.onSelectedTag,
--          modifier = "Alt_L" }
function focus.cycle(args)
  cycler.direction = args.direction
  cycler.filter = args.filter or focus.cycleFilter.onSelectedTag
  cycler.modifier = args.modifier or "Alt_L"
  cycler.actionKey = args.actionKey or "Tab"
  cycler.handle = cycler.onFirstSelect
  cycler.run()
end

return focus
