
local naughty = require("naughty")
local awful = require("awful")
local sys = require("sys")
local gears = require("gears")
local beautiful = require("beautiful")

display = { }

-- debugging
local debug = function(t)end

function notifyDebug(t)
  naughty.notify({ title = "DEBUG", text = t })
end


function display.enableDebug() debug = notifyDebug end

  -- Get active outputs
function display.outputs()
  local outputs = {}
  local xrandr = io.popen("xrandr -q")
  if xrandr then
    for line in xrandr:lines() do
      output = line:match("^([%w-]+) connected ")
      if output then
        outputs[#outputs + 1] = output
      end
    end
    xrandr:close()
  end
  return outputs
end

local function arrange(out)
  -- We need to enumerate all the way to combinate output. We assume
  -- we want only an horizontal layout.
  local choices  = {}
  local previous = { {} }
  for i = 1, #out do
    -- Find all permutation of length `i`: we take the permutation
    -- of length `i-1` and for each of them, we create new
    -- permutations by adding each output at the end of it if it is
    -- not already present.
    local new = {}
    for _, p in pairs(previous) do
      for _, o in pairs(out) do
        if not awful.util.table.hasitem(p, o) then
          new[#new + 1] = awful.util.table.join(p, {o})
        end
      end
    end
    choices = awful.util.table.join(choices, new)
    previous = new
  end
   return choices
end

-- Build available choices
local function menu()
  local menu = {}
  local out = display.outputs()
  local choices = arrange(out)

  for _, choice in pairs(choices) do
    local cmd = "xrandr"
    -- Enabled outputs
    for i, o in pairs(choice) do
      cmd = cmd .. " --output " .. o .. " --auto"
      if i > 1 then
        cmd = cmd .. " --right-of " .. choice[i-1]
      end
    end
    -- Disabled outputs
    for _, o in pairs(out) do
      if not awful.util.table.hasitem(choice, o) then
        cmd = cmd .. " --output " .. o .. " --off"
      end
    end

    local label = ""
    if #choice == 1 then
      label = 'Only <span weight="bold">' .. choice[1] .. '</span>'
    else
      for i, o in pairs(choice) do
        if i > 1 then label = label .. " + " end
        label = label .. '<span weight="bold">' .. o .. '</span>'
      end
    end
    menu[#menu + 1] = { label, cmd }
  end
  return menu
end

-- Display xrandr notifications from choices
local state = { iterator = nil, timer = nil, cid = nil }

function display.cycleSetup()
  -- Stop any previous timer
  if state.timer then
    state.timer:stop()
    state.timer = nil
  end

  -- Build the list of choices
  if not state.iterator then
    state.iterator = awful.util.table.iterate(menu(), function() return true end)
  end

  -- Select one and display the appropriate notification
  local next  = state.iterator()
  local label, action
  if not next then
    label = "Keep the current configuration"
    state.iterator = nil
  else
    label, action = unpack(next)
  end
  state.cid = naughty.notify({ text = label,
    timeout = 4,
    screen = mouse.screen, -- Important, not all screens may be visible
    replaces_id = state.cid }).id

  -- Setup the timer
  state.timer = gears.timer { timeout = 4 }
  state.timer:connect_signal("timeout",
    function()
      state.timer:stop()
      state.timer = nil
      state.iterator = nil
      if action then
        awful.util.spawn(action, false)
      end
    end)
  state.timer:start()
end

local function workareaUdpated()
  debug("Workarea changed")
  debug("connected monitors: " .. table.concat(outputs(), ','))
end

function display.initTracing()
  screen.connect_signal("property::workarea", workareaUdpated)
end

function display.printInfo()
  if screen[1].outputs then
    for name, _ in pairs(screen[1].outputs) do
      debug(tostring(name))
    end
  else
    debug("NO outputs")
  end
end


function display.disableDPM()
  sys.run("xset -dpms")
end

function display.disableScreenSaver()
  sys.run("xset s off")
end

function display.lock(bgColor)
  sys.run("i3lock -c " .. (bgColor or "112222"))
end

-- profile = { name= "profile name",
--             autoSwitch = [true, false],
--             notify = [true, false],
--             outputs = { { name = "xrandr output name",
--                           primary = [true, false], -- optional
--                           cfg=[ "same-as", "right-of", "left-of", "above", "below" ] .. " " .. "xrandr output", "auto", "off" } } }
function display.addProfile()

end


return display
