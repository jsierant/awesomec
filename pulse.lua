local sys = require("sys")
local wibox = require("wibox")
local gears = require("gears")
local beautiful = require("beautiful")
local awful = require("awful")
local mouse_buttons = require('mouse_buttons')
local defaultapps = require('defaultapps')

local pulse = { ctl = {} }

local cmd = "pacmd"
local mult = 0x10000 / 100
local default_vol_step = 2

pulse.ctl.max_vol = 150


function pulse.ctl.update_state()
  local out = sys.collect_output(cmd .. " dump")
  sink = string.match(out, "set%-default%-sink%s+(%S+)")

  for s, v in string.gmatch(out, "set%-sink%-volume%s+(%S+)%s+(0x%x+)") do
    if s == sink then current_volume = (tonumber(v) / mult) break end
  end

  for s, m in string.gmatch(out, "set%-sink%-mute%s+(%S+)%s+([yesno]+)") do
    if s == sink then mute = "yes" == m break end
  end
end

local function setVol(vol)
   sys.run(cmd .. " set-sink-volume " .. sink .. " " .. string.format("0x%x", math.floor(vol * mult)))
end

function pulse.ctl.volume_up(step)
  current_volume = current_volume + (step or default_vol_step)
  if current_volume > pulse.ctl.max_vol then current_volume = pulse.ctl.max_vol end
  setVol(current_volume)
end

function pulse.ctl.volume_down(step)
  current_volume = current_volume - (step or default_vol_step)
  if current_volume < 0 then current_volume = 0 end
  setVol(current_volume)
end

function pulse.ctl.volume()
  return current_volume
end

function pulse.ctl.muted()
  return mute
end

function pulse.ctl.mute(act)
  act = act or "toggle"

  if act == "toggle" then mute = not mute
  else mute = act end

  sys.run(cmd .. " set-sink-mute " .. sink .. " " .. tostring(mute))
end


pulse.shortcuts = awful.util.table.join(
    awful.key({}, "XF86AudioMute", pulse.ctl.mute,
              {description = "Toggle mute", group = "multimedia"}),
    awful.key({}, "XF86AudioLowerVolume", pulse.ctl.volume_down,
              {description = "Lower volume", group = "multimedia"}),
    awful.key({}, "XF86AudioRaiseVolume", pulse.ctl.volume_up,
              {description = "Raise volume", group = "multimedia"})
)

local buttons = awful.util.table.join(
  awful.button({ }, mouse_buttons.left, function () sys.run(defaultapps.audiocontrol) end),
  awful.button({ }, mouse_buttons.middle,  function() pulse.ctl.mute() end),
  awful.button({ }, mouse_buttons.down, function() pulse.ctl.volume_up() end),
  awful.button({ }, mouse_buttons.up, function() pulse.ctl.volume_down() end ))

pulse.widget = wibox.widget {
    widget = wibox.widget.imagebox
}

pulse.widget:buttons(buttons)

function update_view()
  if pulse.ctl.muted() then
    pulse.widget.image = beautiful.audio_volume_muted
    return
  end
  if pulse.ctl.volume() < 30 then
    pulse.widget.image = beautiful.audio_volume_low
    return
  end
  if pulse.ctl.volume() < 60 then
    pulse.widget.image = beautiful.audio_volume_medium
    return
  end
  pulse.widget.image = beautiful.audio_volume_high
end

pulse.timer = gears.timer {
  timeout = 1
}

pulse.timer:connect_signal(
  'timeout',
    function()
      pulse.ctl.update_state()
      update_view()
    end
)

pulse.timer:start()

return pulse
