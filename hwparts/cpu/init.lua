local utils = require('hwparts.utils')

local cpu = {
  core = require('hwparts.cpu.core')
}


local function parse_core_state(line)
  local stat = utils.split(line, "%s+")
  return {
    user = tonumber(stat[2]) + tonumber(stat[3]),
    sys = tonumber(stat[4]),
    idle = tonumber(stat[5]),
    iowait = tonumber(stat[6]),
    irq = tonumber(stat[7]),
  }
end

function cpu.load_aggregated_state()
  local states = { cores = {}}
  local proc_stat = io.lines("/proc/stat")
  if not proc_stat then return nil end
  local idx = 0
  for line in proc_stat do
    if idx == 0 then
      states.total = parse_core_state(line)
    elseif idx > 0 and line:sub(1, 3) == "cpu" then
      table.insert(states.cores, parse_core_state(line))
    else
      break
    end
    idx = idx + 1
  end
  return states
end

local function make_null_state(core_count)
  local state = { total = cpu.core.null_state, cores = {}}
  for _=1,core_count do
    table.insert(state.cores, cpu.core.null_state)
  end
  return state
end

function cpu.diff(prev, new)
  local state = { total = cpu.core.diff(prev.total, new.total), cores = {}}
  for idx=1,#new.cores do
    table.insert(state.cores, cpu.core.diff(prev.cores[idx], new.cores[idx]))
  end
  return state
end

local last_state = nil

function cpu.state()
  local new = cpu.load_aggregated_state()
  if not last_state then
    last_state = new
    return make_null_state(#last_state.cores)
  end
  local actual = cpu.diff(last_state, new)
  last_state = new
  return actual
end

return cpu

