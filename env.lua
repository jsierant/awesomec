
local awful = require("awful")

local env = {}

env.xdg_config_home = os.getenv("XDG_CONFIG_HOME")
env.config_dir = awful.util.getdir("config")
env.theme_dir = env.config_dir .. "theme"

env.primary_screen=1

return env
