local core = {}


core.null_state = {
  user = 0, sys = 0, idle = 0, iowait = 0, irq = 0
}

function core.diff(prev, new)
  return {
    user = new.user - prev.user,
    sys = new.sys - prev.sys,
    idle = new.idle - prev.idle,
    iowait = new.iowait - prev.iowait,
    irq = new.irq - prev.irq,
  }
end

function core.to_string(state)
  return string.format('user:%s, sys:%s, idle:%s, iowait:%s, irq:%s',
    state.user, state.sys, state.idle, state.iowait, state.irq)
end

--- Calculate total core usage
--@tparam table state
--@treturn number total core usage in [%]
function core.total(state)
  local sum = state.user +
              state.sys +
              state.idle +
              state.iowait +
              state.irq
  return 100 * (state.user +
                state.sys) / sum
end

return core
