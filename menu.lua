local hotkeys_popup = require("awful.hotkeys_popup").widget
local defaultapps = require('defaultapps')
local beautiful = require("beautiful")
local awful = require("awful")

menu = {}

-- Create a launcher widget and a main menu
menu.awesome = {
   { "hotkeys", function() return false, hotkeys_popup.show_help end},
   { "manual", defaultapps.terminal .. " -e man awesome" },
   { "edit config", defaultapps.editor .. " " .. awesome.conffile },
   { "restart", awesome.restart },
   { "quit", function() awesome.quit() end}
}

menu.main =
awful.menu({
    items = {
      { "awesome", menu.awesome , beautiful.awesome_icon },
      { "terminal", defaultapps.terminal }
    }
  })

menu.launcher =
  awful.widget.launcher({
    image = beautiful.awesome_icon,
    menu = menu.main
  })

menu.show = function()
    menu.main:show()
end

return menu
