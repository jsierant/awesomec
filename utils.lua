
local awful = require("awful")

local utils = {}

local globalkeys = {}

function utils.addGlobalShortcuts(keys)
  globalkeys = awful.util.table.join(globalkeys, keys)
end

function utils.activateGlobalShortcuts()
  root.keys(globalkeys)
end


function utils.foreach(tab, fun)
    for _, v in pairs(tab) do
        fun(v)
    end
end


return utils
