local sys = require("sys")

local naughty = require("naughty")
ctrl={ song = nil }

local basic_cmd="mpc"
local player_cmd="termite -e /usr/bin/ncmpcpp"
local vol_default_step = 2

function run_player(command)
  sys.run(basic_cmd .. " " .. command)
end

function ctrl.stop()
  run_player("stop")
end

function ctrl.show_player()
  sys.run(player_cmd)
end

function ctrl.toggle_play()
  run_player("toggle")
end

function ctrl.play_next()
  run_player("next")
end

function ctrl.play_prev()
  run_player("prev")
end

function ctrl.volume_up(step)
  run_player("volume +" .. tostring(step or vol_default_step))
end

function ctrl.volume_down(step)
  run_player("volume -" .. tostring(step or vol_default_step))
end

function ctrl.song_info()
    return sys.collect_output(basic_cmd .. " current"):gsub("\n", "")
end


function ctrl.update()
--   ZZ Top - Legs
-- [playing] #53/54   0:04/4:32 (1%)
-- volume: 70%   repeat: off   random: off   single: off   consume: off
  ctrl.song, ctrl.status, progress, volume = string.match(sys.collect_output(basic_cmd), "(.*)\n%[(.*)%].*%((%d+)%%%).*volume:%s*([%dn/a]+)")

  if not progress then progress = 0 end
  ctrl.progress = tonumber(progress)

  if not volume or volume == "n/a" then volume = 0 end
  ctrl.volume = tonumber(volume)
--   naughty.notify({ title = "mpd status", text = "song: " .. tostring(ctrl.song) .. "\nstatus: " .. tostring(ctrl.status) .. "\nprogress: " .. tostring(ctrl.progress) .. "\nvolume: " .. tostring(ctrl.volume) })
end

return ctrl
