#!/bin/sh

#stop running session
/usr/bin/tmux send-keys -t rtorrent C-q && while pidof rtorrent > /dev/null; do sleep 0.5; done

# start a new session
/usr/bin/tmux new-session -s rt -n rtorrent -d rtorrent

